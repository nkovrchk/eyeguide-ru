const singleChoiceInputs = new Choices('.single-choice'),
    multipleChoiceInputs = new Choices('.multiple-choice', {
        removeItemButton: true
    });